import { Post } from "../entity/Post";
import { connection } from "./connection";

export class PostRepository {

    static async add(post,user) {
        const [row] = await connection.execute('INSERT INTO post (date_time,author,title,text,likes,picture,user_id) VALUES (?,?,?,?,?,?,?)', [post.dateTime,user.username,post.title,post.text,post.like,post.picture,user.id]);
        return post.id= row.insertId;
    };

    static async delete(id){
        await connection.execute(`DELETE FROM post WHERE id=?`,[id]);
    };

    static async update(post){
        await connection.query('UPDATE post SET title=?, text=?, picture=? WHERE id=?', [post.title, post.text, post.picture, post.id]);
    };

    static async getAllPosts(){
        const [rows] = await connection.execute('SELECT * FROM post ORDER BY date_time DESC');
        return rows.map(item => new Post(item['date_time'],item['author'], item['title'],item['text'],item['likes'],item['picture'],item['user_id'],item['id']));
    };

    static async getUserPosts(userId){
        const [rows] = await connection.execute('SELECT * FROM post WHERE user_id=? ORDER BY date_time DESC',[userId]);
        return rows.map(item=> new Post(item['date_time'],item['author'],item['title'],item['text'],item['likes'],item['picture'],item['user_id'],item['id']));
    }

    static async findById(id) {
        let [row] = await connection.execute(`SELECT * FROM post WHERE id=?`, [id]);
        return new Post(row[0]['date_time'],row[0]['author'],row[0]['title'],row[0]['text'],row[0]['likes'],row[0]['picture'],row[0]['user_id'],row[0]['id']);
    }

    static async search(search){
        const [rows] = await connection.execute(`SELECT * FROM post WHERE CONCAT(author,title) LIKE ? ORDER BY date DESC`, ['%'+search+'%']);
        return rows.map(item=> new Post(item['date_time'],item['author'],item['title'],item['text'],item['likes'],item['picture'],item['user_id'],item['id']));
    };

    static async findLike(userId, postId){
        const [row] = await connection.execute('SELECT * FROM post_like WHERE user_id=? AND post_id=?', [userId,postId])
        return row[0]
    } 

    static async like(userId,postId){
        await connection.execute('INSERT INTO post_like (user_id,post_id) VALUES(?,?)',[userId,postId]);
        await connection.execute('UPDATE post SET likes=(select COUNT(*) from post_like WHERE post_id=?) WHERE id=?',[postId,postId])
    }

    static async dislike(userId,postId){
        await connection.execute('DELETE FROM post_like WHERE user_id=? AND post_id=?',[userId,postId]);
        await connection.execute('UPDATE post SET likes=(select COUNT(*) from post_like WHERE post_id=?) WHERE id=?',[postId,postId])
    }
}
