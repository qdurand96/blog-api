import { User } from "../entity/User";
import { connection } from "./connection";

export class UserRepository {

    static async add(user) {
        const [rows] = await connection.execute('INSERT INTO user (username,email,password,role,picture) VALUES (?,?,?,?,?)', [user.username, user.email, user.password, user.role, user.picture]);
        user.id= rows.insertId;
    };

    static async delete(id){
        await connection.execute(`DELETE FROM user WHERE id=?`,[id])
    };

    static async update(user){
        await connection.execute('UPDATE user SET username=?, email=?, password=?, role=?, picture=? WHERE id=?', [user.username, user.email, user.password, user.role, user.picture, user.id]);
        await connection.execute('UPDATE post SET author=? WHERE user_id=?',[user.username,user.id])
    };

    static async findByEmail(email) {
        const [rows] = await connection.execute('SELECT * FROM user WHERE email=?', [email]);
        if(rows.length === 1) {
            return new User(rows[0].username, rows[0]['email'], rows[0]['password'], rows[0]['role'], rows[0]['picture'], rows[0]['id']);
        };
        return null;
    };

    static async findById(id) {
        let [rows] = await connection.execute(`SELECT * FROM user WHERE id=?`, [id]);
        return new User(rows[0].username, rows[0]['email'], rows[0]['password'], rows[0]['role'], rows[0]['picture'], rows[0]['id']);
    }
}