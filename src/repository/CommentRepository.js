import { Comment } from "../entity/Comment";
import { connection } from "./connection";

export class CommentRepository {

    static async add(comment,user) {
        const [rows] = await connection.execute('INSERT INTO comment (date_time,author,text,likes,user_id,post_id) VALUES (?,?,?,?,?,?)', [comment.dateTime,user.username,comment.text,comment.like,user.id,comment.postId]);
        return comment.id= rows.insertId;
    };

    static async delete(id){
        await connection.execute(`DELETE FROM comment WHERE id=?`,[id])
    };

    static async update(comment){
        await connection.execute('UPDATE comment SET text=? WHERE id=?', [comment.text, comment.id]);
    };

    static async findById(id) {
        let [row] = await connection.execute(`SELECT * FROM comment WHERE id=?`, [id]);
        return new Comment(row[0]['date_time'],row[0]['author'],row[0]['text'],row[0]['likes'],row[0]['user_id'],row[0]['post_id'],row[0]['id']);
    } 

    static async getCommentsForPost(postId){
        const [rows] = await connection.execute('SELECT * from comment WHERE post_id=? ORDER BY date_time DESC',[postId]);
        return rows.map(item => new Comment(item['date_time'],item['author'],item['text'],item['likes'],item['user_id'],item['post_id'],item['id']));
    };

    static async findLike(userId, commentId){
        const [row] = await connection.execute('SELECT * FROM comment_like WHERE user_id=? AND comment_id=?', [userId,commentId])
        return row[0]
    }

    static async like(userId,commentId){
        await connection.execute('INSERT INTO comment_like (user_id,comment_id) VALUES(?,?)',[userId,commentId]);
        await connection.execute('UPDATE comment SET likes=(select COUNT(*) from comment_like WHERE comment_id=?) WHERE id=?',[commentId,commentId])
    }

    static async dislike(userId,commentId){
        await connection.execute('DELETE FROM comment_like WHERE user_id=? AND comment_id=?',[userId,commentId]);
        await connection.execute('UPDATE comment SET likes=(select COUNT(*) from comment_like WHERE comment_id=?) WHERE id=?',[commentId,commentId])
    }
}