import { Router } from "express";
import { PostRepository } from "../repository/PostRepository";
import passport from "passport";
import { CommentRepository } from "../repository/CommentRepository";
import { Post } from "../entity/Post";
import { uploader } from "../uploader";
import { convertUTCDateToLocalDate } from "../dateConverter";

export const postController = Router();

postController.get('/', async (req, res) => {
    try {
        let posts;
        if (req.query.search) {
            posts = await PostRepository.search(req.query.search);
        } else {
            posts = await PostRepository.getAllPosts();
        }
        res.json(posts)
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})

postController.get('/:id', async (req, res) => {
    try {
        res.json(await PostRepository.findById(req.params.id))
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})

postController.get('/myposts/all', passport.authenticate('jwt', { session: false }), async (req,res)=>{
    try {
        res.json(await PostRepository.getUserPosts(req.user.id))
    } catch (error) {
        res.status(400).end()
    }
})

//ajouter un post
postController.post('/', passport.authenticate('jwt', { session: false }), uploader.single('picture'), async (req, res) => {
    try {
        const newPost = new Post();
        Object.assign(newPost, req.body);
        if (req.file) {
            newPost.picture = '/uploads/' + req.file.filename;
        }
        let date = convertUTCDateToLocalDate(new Date());
        newPost.dateTime = date.toISOString().slice(0, 19).replace('T', ' ');
        await PostRepository.add(newPost, req.user);
        res.status(201).json(newPost);
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
});

//modifier
postController.patch('/:id', passport.authenticate('jwt', { session: false }), uploader.single('picture'), async (req, res) => {
    try {
        let data = await PostRepository.findById(req.params.id);
        if (!data) {
            res.send(404).end();
            return
        }
        if (data.userId !== req.user.id) {
            res.send(401).end();
            return
        }
        let update = { ...data, ...req.body };
        if (req.file) {
            update.picture = '/uploads/' + req.file.filename;
        }
        let date = convertUTCDateToLocalDate(new Date());
        update.dateTime = date.toISOString().slice(0, 19).replace('T', ' ');
        await PostRepository.update(update);
        res.json(update);
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})

postController.delete('/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        let toDelete = await PostRepository.findById(req.params.id);
        if (!toDelete) {
            res.status(404);
            return
        }
        if (req.user.id === toDelete.userId || req.user.role === 'admin') {
            await PostRepository.delete(toDelete.id);
            res.status(204).end();
        } else {
            res.status(401).end();
        }
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
})

postController.get('/:id/comment', async (req, res) => {
    try {
        res.json(await CommentRepository.getCommentsForPost(req.params.id))
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})

postController.post('/:id/like', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        const liked = await PostRepository.findLike(req.user.id, req.params.id)
        if (liked) {
            await PostRepository.dislike(req.user.id, req.params.id);
            res.status(204).end()
        } else {
            await PostRepository.like(req.user.id, req.params.id);
            res.status(201).end()
        }
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})

postController.get('/:id/like',passport.authenticate('jwt', { session: false }), async (req, res) =>{
    try {
        const liked = await PostRepository.findLike(req.user.id, req.params.id)
        if(liked){
            res.status(200).json(true)
        } else {
            res.status(200).json(false)
        }
    } catch (error) {
        console.log(error);
        res.status(400).end()
    }
})
