import { Router } from "express";
import { User, userSchema } from "../entity/User";
import { UserRepository } from "../repository/UserRepository";
import bcrypt from 'bcrypt';
import { generateToken } from "../utils/token";
import passport from "passport";
import { uploader } from "../uploader";

import fs from 'fs'
import path from 'path';

export const userController = Router();

userController.post('/register',uploader.single('picture'), async (req, res) => {
    try {
        const {error} = userSchema.validate(req.body, {abortEarly:false});
        
        if(error) {
            res.status(400).json({error: error.details.map(item => item.message)});
            return;
        }

        const newUser = new User();
        Object.assign(newUser, req.body);
        
        const exists = await UserRepository.findByEmail(newUser.email);
        if(exists) {
            res.status(400).json({error: 'Email already taken'});
            return;
        }
        if(req.file){
            newUser.picture = '/uploads/'+req.file.filename;
        }
        //On assigne user en role pour pas qu'un user puisse choisir son rôle à l'inscription
        newUser.role = 'user';
        //On hash le mdp du user pour pas le stocker en clair
        newUser.password = await bcrypt.hash(newUser.password, 11);

        await UserRepository.add(newUser);

        res.status(201).json({
            user: newUser,
            token: generateToken({
                email: newUser.email,
                id:newUser.id,
                role:newUser.role
            })
        });

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});


userController.post('/login', async (req,res) => {
    try{
        const user = await UserRepository.findByEmail(req.body.email);
        if(user) {
            const samePassword = await bcrypt.compare(req.body.password, user.password);
            if(samePassword) {
                res.json({
                    user,
                    token: generateToken({
                        email: user.email,
                        id:user.id,
                        role:user.role
                    })
                });
                return;
            }
        }
        res.status(401).json({error: 'Wrong email and/or password'});
    }catch(error) {
        console.log(error);
        res.status(500).json(error);
    }
});


userController.get('/account', passport.authenticate('jwt', {session:false}), (req,res) => {
    res.json(req.user);
});

userController.patch('/account', passport.authenticate('jwt', {session:false}), uploader.single('picture'), async(req,res) => {
    try {
        let data = await UserRepository.findById(req.user.id);
        if (!data) {
            res.send(404).end();
            return
        }
        let update = { ...data, ...req.body };
        if(req.file){
            update.picture = '/uploads/'+req.file.filename; 
        }
        await UserRepository.update(update);
        res.json(update);
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})