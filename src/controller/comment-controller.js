import { Router } from "express";
import { CommentRepository } from "../repository/CommentRepository";
import passport from "passport";
import { Comment } from "../entity/Comment";
import { uploader } from "../uploader";
import { convertUTCDateToLocalDate } from "../dateConverter";

export const commentController = Router();

commentController.post('/', passport.authenticate('jwt', { session: false }), uploader.single('picture'), async (req, res) => {
    try {
        const newComment = new Comment();
        Object.assign(newComment, req.body);
        let date = convertUTCDateToLocalDate(new Date());
        newComment.dateTime = date.toISOString().slice(0, 19).replace('T', ' ');
        const id = await CommentRepository.add(newComment, req.user);
        res.status(201).json(await CommentRepository.findById(id));
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
});

commentController.get('/:id',passport.authenticate('jwt', { session: false }), async (req, res) =>{
    try {
        let data = await CommentRepository.findById(req.params.id);
        if (!data) {
            res.send(404).end();
            return
        }
        res.status(200).json(data)
    } catch (error) {
        
    }
})

//modifier
commentController.patch('/:id', passport.authenticate('jwt', { session: false }),uploader.single('picture'), async (req, res) => {
    try {
        let data = await CommentRepository.findById(req.params.id);
        if (!data) {
            res.send(404).end();
            return
        }
        if (data.userId !== req.user.id) {
            res.send(401).end();
            return
        }
        let update = { ...data, ...req.body };
        let date = convertUTCDateToLocalDate(new Date());
        update.dateTime = date.toISOString().slice(0, 19).replace('T', ' ');
        await CommentRepository.update(update);
        res.json(update);
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})

commentController.delete('/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        let toDelete = await CommentRepository.findById(req.params.id);
        if (!toDelete) {
            res.status(404);
            return
        }
        if (req.user.id === toDelete.userId || req.user.role === 'admin') {
            await CommentRepository.delete(toDelete.id);
            res.status(204).end();
        } else {
            res.status(401).end();
        }
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
})

commentController.post('/:id/like', passport.authenticate('jwt', { session: false }), async(req,res)=>{
    try {
        const liked = await CommentRepository.findLike(req.user.id,req.params.id);
        if(liked) {
            await CommentRepository.dislike(req.user.id,req.params.id);
            res.status(204).end()
        } else {
            await CommentRepository.like(req.user.id,req.params.id);
            res.status(201).end()
        }
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})

commentController.get('/:id/like',passport.authenticate('jwt', { session: false }), async (req, res) =>{
    try {
        const liked = await CommentRepository.findLike(req.user.id, req.params.id)
        if(liked){
            res.status(200).json(true)
        } else {
            res.status(200).json(false)
        }
    } catch (error) {
        console.log(error);
        res.status(400).end()
    }
})
