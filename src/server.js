import express from 'express';
import cors from 'cors';
import { userController } from './controller/user-controller';
import { postController } from './controller/post-controller';
import { configurePassport } from './utils/token';
import passport from 'passport';
import { commentController } from './controller/comment-controller';


configurePassport();

export const server = express();

server.use(passport.initialize());

server.use(express.json());
server.use(cors());

server.use(express.static('public'));
server.use('/api/user', userController);
server.use('/api/post', postController);
server.use('/api/comment', commentController);