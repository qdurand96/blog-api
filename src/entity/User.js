import Joi from "joi";

export class User {
    id;
    username;
    email;
    password;
    role;
    picture;

    constructor(username,email,password,role=null,picture=null,id){
        this.username=username;
        this.email=email;
        this.password=password;
        this.role=role;
        this.picture=picture;
        this.id=id;
    }
}

export const userSchema = Joi.object({
    username: Joi.string().min(3).required(),
    email: Joi.string().email().required(),
    password: Joi.string().min(4).required()
});
