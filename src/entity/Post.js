export class Post {
    id;
    dateTime;
    author;
    title;
    text;
    like;
    picture;
    userId;

    constructor(dateTime,author,title,text,like=0,picture=null,userId,id){
        this.dateTime=dateTime;
        this.author=author;
        this.title=title;
        this.text=text;
        this.like=like;
        this.picture=picture;
        this.userId=userId;
        this.id=id;
    }
}