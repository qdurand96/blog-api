export class Comment {
    id;
    dateTime;
    author;
    text;
    like;
    userId;
    postId;

    constructor(dateTime,author,text,like=0,userId,postId,id){
        this.dateTime=dateTime;
        this.author=author;
        this.text=text;
        this.like=like;
        this.userId=userId;
        this.postId=postId;
        this.id=id;
    }
}